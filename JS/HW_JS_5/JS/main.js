"use strict";
/*
Задание
Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
*/

function createNewUser() {
  let newUser, age, password;
  let currentDate = new Date();
  newUser = {
    firstName: prompt("Enter name"),
    lastName: prompt("Enter surname"),
    birthday: prompt("Enter your birthday date (dd.mm.yyyy)"),
    getLogin: function() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge: function () {
      let currentBirthday = new Date(newUser.birthday.split(".").reverse().join(":")); //преобразую в массив, разворачиваю в обратном порядке, возвращаю строку 
      return age = parseInt((currentDate - currentBirthday)/1000/3600/24/365.25); // разницу дат перевожу из секунд в года
    },
    getPassword: function () {
      let year = new Date(newUser.birthday.split(".").reverse().join(":")).getFullYear(); 
      password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + year;
      return password;
    },
    
  }
  console.log(`Your login:  ${newUser.getLogin()}
   Your age: ${newUser.getAge()}
   Your password: ${newUser.getPassword()}`);
  return newUser;
}
createNewUser();
